﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.UI.WebControls;
using WindowsFormsApp1.Modelo;
using WindowsFormsApp1.Vista;

namespace WindowsFormsApp1
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ArrayList lista = new ArrayList();

            List<string> listmem = new List<string>();

            ArrayList recursos = new ArrayList();

            List<string> Nuevo = new List<string>();

            String rec = "";

            Recursos recur = new Recursos();

            Random time = new Random();

            int cont = 0;
            int tamaño1 = 0;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Form1 q = new Form1();
            Form3 u = new Form3();
            Form4 t = new Form4();
            Form5 v = new Form5();
            Form6 w = new Form6();
            q.getDet().Visible = false;
            q.getUt().Visible = false;
            q.getUts().Visible = false;
            q.getButton().Click += (s, e) =>
            {
                q.getProcesos().Text = "";
                Form2 r = new Form2();
                r.Show();
                r.getInfoVer().Visible = false;
                r.getIdP().Enabled = false;
                r.getButton().Visible = false;
                cont++;
                r.getIdP().Text = cont + "";
                r.getVerificar().Click += (_s, _e) =>
                {
                    int x = int.Parse(r.getTam().Text);
                    int y = int.Parse(t.getTamDisp().Text);
                    if ((x <= y) && (x % 10 == 0))
                    {
                        r.getButton().Visible = true;
                        r.getInfoVer().Visible = false;
                        r.getButton().Click += (s_, e_) =>
                        {
                            String nom = r.getNom().Text;
                            int tamaño = int.Parse(r.getTam().Text);
                            Proceso p = new Proceso(cont, nom, tamaño, "Listo", time.Next(1, 10));
                            lista.Add(p);
                            for (int k = 0; k < r.getRec().CheckedItems.Count; k++)
                            {
                                if (rec != "")
                                {
                                    rec += r.getRec().CheckedItems[k].ToString() + ",";
                                    
                                }
                                else
                                {
                                    rec += r.getRec().CheckedItems[k].ToString() + ",";

                                }
                            }
                           
                            recursos.Add(rec);

                            // metodo para saber que recurso se esta utilizando 
                            if (rec.Contains("R1"))
                            {
                                recur.R1 = 0;
                            }
                            if (rec.Contains("R2"))
                            {
                                recur.R2 = 0;
                            }
                            if (rec.Contains("R3"))
                            {
                                recur.R3 = 0;
                            }
                            if (rec.Contains("R4"))
                            {
                                recur.R4 = 0;
                            }
                            if (rec.Contains("R5"))
                            {
                                recur.R5 = 0;
                            }
                            if (rec.Contains("R6"))
                            {
                                recur.R6 = 0;
                            }                            
                            /*MessageBox.Show("R1:" + recur.R1
                                + "\nR2" + recur.R2
                                + "\nR3" + recur.R3
                                + "\nR4" + recur.R4
                                + "\nR5" + recur.R5
                                + "\nR6" + recur.R6
                                );*/
                            Nuevo.Add(nom);
                            r.Dispose();
                            q.getProcesos().Text += " ";
                            q.getNuevo().Text = "";
                            int acum = 0;
                            for (int i = 0; i < lista.Count; i++)
                            {

                                Proceso pr = (Proceso)lista[i];
                                q.getTiempo().Text = pr.getTime() + " Segundos";
                                q.getProcesos().Text += pr.getId() + "\t";
                                q.getProcesos().Text += pr.getNom() + "\t" + "\t";
                                q.getProcesos().Text += pr.getTam() + " Kb" + "\t" + "\t";
                                q.getProcesos().Text += pr.getTime() + " seg" + "\t" + "\t";
                                q.getProcesos().Text += "Estado" + "\t" + "\t";
                                q.getProcesos().Text += "       " + recursos[i];
                                q.getProcesos().Text += "\r\n";
                                acum += pr.getTam();
                                q.getNuevo().Text += "\t" + Nuevo[i] + "\r\n";
                            }
                            tamaño1 = 2000 - acum;


                        };
                        rec = "";
                    }
                    else
                    {
                        r.getInfoVer().Visible = true;
                        r.getNom().Text = "";
                        r.getTam().Text = "";
                        for (int j = 0; j < r.getRec().Items.Count; j++)
                        {
                            r.getRec().SetItemChecked(j, false);
                        }

                    }
                };

            };
            //boton ejecutar
            q.getEjec().Click += (s, e) =>
            {
                if (lista.Count == 0)
                {
                    v.Show();
                }
                else
                {
                    q.getDet().Visible = true;
                    q.getEjec().Visible = false;
                    q.getUt().Visible = true;
                    q.getUts().Visible = true;
                    q.getMem().Enabled = false;
                    q.getButton().Enabled = false;
                    q.getacerca().Enabled = false;
                    /*q.getListo().Text = q.getNuevo().Text;
                    q.getNuevo().Text = "";*/
                    
                    q.getDet().Click += (s_, e_) =>
                    {
                        q.getDet().Visible = false;
                        q.getEjec().Visible =true;
                        q.getMem().Enabled = true;
                        q.getButton().Enabled = true;
                        q.getacerca().Enabled = true;
                        q.getUt().Visible = false;
                        q.getUts().Visible = false;
                    };


                }
            };
           
            q.getMem().Click += (s, e) =>
            {
                t.Show();
                t.getTamDisp().Text = "" + tamaño1;
            };
            q.getacerca().Click += (s, e) =>
            {
                u.Show();
            };
            q.getRecursos().Click += (s, e) =>
            {
                string FL;

                //recurso 1
                FL = Recursoutil(recur.R1);
                w.getr1().Text = FL;
                //recurso 2
                FL = Recursoutil(recur.R2);
                w.getr2().Text = FL;
                //recurso 3
                FL = Recursoutil(recur.R3);
                w.getr3().Text = FL;
                //recurso 4
                FL = Recursoutil(recur.R4);
                w.getr4().Text = FL;
                //recurso 5
                FL = Recursoutil(recur.R5);
                w.getr5().Text = FL;
                //recurso 6
                FL = Recursoutil(recur.R6);
                w.getr6().Text = FL;
                w.Show();

            };
            Application.Run(q);


        }
        public static String Recursoutil(int a)
        {

            string c;
            if (a == 1)
            {
                c = "Disponible";
            }
            else
            {
                c = "No Disponible";
            }

            return c;
        }
        
    }

}
