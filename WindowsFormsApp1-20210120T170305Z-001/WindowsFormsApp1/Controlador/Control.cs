﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.UI.WebControls;
using WindowsFormsApp1.Modelo;
using WindowsFormsApp1.Vista;
using System.Threading;
using System.Security.Cryptography;

namespace WindowsFormsApp1
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ArrayList lista = new ArrayList();//lista de procesos
            //List<string> listmem = new List<string>();
            ArrayList recursos = new ArrayList();//lista recursos

            List<string> Nuevo = new List<string>();//nuevo proceso que se crea
            
            String rec = "";
            string listaauxiliar = "";

            List<String[]> list = new List<string[]>();
            
            String[] vs = { "a ", "s " };
            
            Recursos1 recur = new Recursos1();
            
            Random time = new Random();
            int cont = 0;
            int tamaño1 = 0;

            //formulariuos
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Form1 q = new Form1();
            Form3 u = new Form3();
            Form4 t = new Form4();
            Form5 v = new Form5();
            Form6 w = new Form6();

            q.getDet().Visible = false;//boton deterne

            //boton crear
            q.getButton().Click += (s, e) =>
            {
                q.getProcesos().Text = "";
                Form2 r = new Form2();
                r.Show();
                r.getInfoVer().Visible = false;
                r.getIdP().Enabled = false;
                r.getButton().Visible = false;
                cont++;
                r.getIdP().Text = cont + "";

                //boton verificar
                r.getVerificar().Click += (_s, _e) =>
                {
                    int x = int.Parse(r.getTam().Text);//tamaño de memoria que se coloca
                    int y = int.Parse(t.getTamDisp().Text);//tamaño de memoria disponible

                    if ((x <= y) && (x % 10 == 0))//valida que sea par y que no sea mayor al tamaño de memoria
                    {
                        r.getButton().Visible = true;
                        r.getInfoVer().Visible = false;

                        r.getButton().Click += (s_, e_) =>
                        {
                            String nom = r.getNom().Text;//nombre recurso
                            int tamaño = int.Parse(r.getTam().Text);//tamaño recurso

                            Proceso p = new Proceso(cont, nom, tamaño, "Listo", time.Next(1, 10));
                            lista.Add(p); //agrego proceso a la lista

                            //visualiza el tipo de recurso en formulario
                            for (int k = 0; k < r.getRec().CheckedItems.Count; k++)
                            {
                                if (rec != "")
                                {
                                    rec += r.getRec().CheckedItems[k].ToString() + ",";
                                }
                                else
                                {
                                    rec += r.getRec().CheckedItems[k].ToString() + ",";

                                }
                            }

                            recursos.Add(rec);//adiciono a la lista de recursos el recurso

                            {
                                // metodo para saber que recurso se esta utilizando 
                                /*  if (rec.Contains("R1"))
                                  {
                                      recur.R1 = 0;
                                  }
                                  if (rec.Contains("R2"))
                                  {
                                      recur.R2 = 0;
                                  }
                                  if (rec.Contains("R3"))
                                  {
                                      recur.R3 = 0;
                                  }
                                  if (rec.Contains("R4"))
                                  {
                                      recur.R4 = 0;
                                  }
                                  if (rec.Contains("R5"))
                                  {
                                      recur.R5 = 0;
                                  }
                                  if (rec.Contains("R6"))
                                  {
                                      recur.R6 = 0;
                                  }      */

                                /*MessageBox.Show("R1:" + recur.R1
                                    + "\nR2" + recur.R2
                                    + "\nR3" + recur.R3
                                    + "\nR4" + recur.R4
                                    + "\nR5" + recur.R5
                                    + "\nR6" + recur.R6
                                    );*/
                            }

                            Nuevo.Add(nom);//adiciona el nombre de proceso

                            r.Dispose();
                            q.getProcesos().Text += " ";
                            q.getNuevo().Text = "";

                            int acum = 0;
                            string listpro = "";
                            
                            list.Clear();

                            //recorro la lista de procesos
                            for (int i = 0; i < lista.Count; i++)
                            {
                                Proceso pr = (Proceso)lista[i];//asigno proceso a pr

                                //muestro en vista
                                q.getTiempo().Text = pr.getTime() + " Segundos";
                                q.getProcesos().Text += pr.getId() + "\t";
                                q.getProcesos().Text += pr.getNom() + "\t" + "\t";
                                q.getProcesos().Text += pr.getTam() + " Kb" + "\t" + "\t";
                                q.getProcesos().Text += pr.getTime() + " seg" + "\t" + "\t";
                                q.getProcesos().Text += pr.getEstado() + "\t" + "\t";
                                q.getProcesos().Text += "       " + recursos[i];
                                q.getProcesos().Text += "\r\n";

                                //agrego todo a la lista de procesos listpro
                                listpro += pr.getId() + "-";
                                listpro += pr.getNom() + "-";
                                listpro += pr.getTam() + "-";
                                listpro += pr.getTime() + "-";
                                listpro += pr.getEstado() + "-";
                                listpro += "" + recursos[i] + "-";

                                acum += pr.getTam();
                                q.getNuevo().Text += "\t" + Nuevo[i] + "\r\n";

                                listaauxiliar += listpro;
                               
                                vs = listaauxiliar.Split('-');
                                list.Add(vs);
                                listaauxiliar = " ";

                                listpro = " ";

                            }
                            tamaño1 = 2000 - acum;                            
                        };
                        rec = "";

                    }
                    else
                    {
                        r.getInfoVer().Visible = true;
                        r.getNom().Text = "";
                        r.getTam().Text = "";
                        for (int j = 0; j < r.getRec().Items.Count; j++)
                        {
                            r.getRec().SetItemChecked(j, false);
                        }

                    }
                };

            };

            //boton ejecutar
            q.getEjec().Click += (s, e) =>
            {   //si lista vacia muestro formulario 5
                if (lista.Count == 0)
                {
                    v.Show();
                }
                else
                {
                    q.getDet().Visible = true;
                    q.getEjec().Visible = false;
                    q.getMem().Enabled = false;
                    q.getButton().Enabled = false;
                    q.getacerca().Enabled = false;
                    q.getListo().Text = "";
                    q.getbloqueados().Text = "";
                    q.getEjecu().Text = "";
                    q.getterminar().Text = "";

                    //pasa los nombres de los procesos a la caja de texto listo
                    foreach (string[] word in list)
                    {
                        {   //muestra lo que tiene cada proceso y a donde pasa - listo -
                            string fdle ="";
                            for (int i= 0 ; i < word.Length; i++){
                                fdle += word[i]+ " ";
                            }
                            MessageBox.Show(fdle);
                            //fdle = "";
                        }
                        q.getNuevo().Text = " ";
                        q.getListo().Text += word[1] + "\r\n";
                        word[4] = "listo";
                    }
                    //esperar 5 seg
                    Thread.Sleep(1000);
                    //pasa los nombre de los procesos a la caja de texto ejecutando
                    foreach (string[] word in list)
                    {
                        q.getListo().Text = " ";
                        q.getEjecu().Text += word[1] + "\r\n";
                        string recursox = word[5]; //asigna el recurso del proceso a recursox
                        Thread.Sleep(1000);

                        //validaciones recursos no disponibles
                        if (word[5].Contains("R1"))//toma el recurso 
                        {
                            if (recur.R1 == 0) //valida que sea 0 - no disponible -
                            {
                                q.getbloqueados().Text += word[1] + "\r\n";
                            }
                        }
                        if (word[5].Contains("R2"))
                        {
                            if (recur.R2 == 0)
                            {
                                q.getbloqueados().Text += word[1] + "\r\n";
                            }
                        }
                        if (word[5].Contains("R3"))
                        {
                            if (recur.R3 == 0)
                            {
                                q.getbloqueados().Text += word[1] + "\r\n";
                            }
                        }
                        if (word[5].Contains("R4"))
                        {
                            if (recur.R4 == 0)
                            {
                                q.getbloqueados().Text += word[1] + "\r\n";
                            }
                        }
                        if (word[5].Contains("R5"))
                        {
                            if (recur.R5 == 0)
                            {
                                q.getbloqueados().Text += word[1] + "\r\n";
                            }
                        }
                        if (word[5].Contains("R6"))
                        {
                            if (recur.R6 == 0)
                            {
                                q.getbloqueados().Text += word[1] + "\r\n";
                            }
                        }
                        
                        //transformar a 0 el recurso
                        if (word[5].Contains("R1"))
                        {
                            recur.R1 = 0;
                        }
                        if (word[5].Contains("R2"))
                        {
                            recur.R2 = 0;
                        }
                        if (word[5].Contains("R3"))
                        {
                            recur.R3 = 0;
                        }
                        if (word[5].Contains("R4"))
                        {
                            recur.R4 = 0;
                        }
                        if (word[5].Contains("R5"))
                        {
                            recur.R5 = 0;
                        }
                        if (word[5].Contains("R6"))
                        {
                            recur.R6 = 0;
                        }
                        
                    }

                    foreach(string[] word in list){ 
                        if (q.getbloqueados().ToString().Contains(word[1]))//nombre del proceso
                        {
                            q.getEjecu().Text = q.getEjecu().Text.Replace(word[1], string.Empty);
                            MessageBox.Show("entro en if");

                            int wo;
                            wo = int.Parse(word[3]);//se asigna tiempo en - wo -
                            wo = wo * 1000;
                            
                            Thread.Sleep(wo);//tiempo de espera - ejecucion -

                            q.getterminar().Text += word[1] + "\r\n";//nombre del proceso

                            //transforsmar en 1 el recurso
                            if (word[5].Contains("R1"))
                            {
                                recur.R1 = 1;
                                MessageBox.Show("r1 " + recur.R1);
                            }
                            if (word[5].Contains("R2"))
                            {
                                recur.R2 = 1;
                                MessageBox.Show("r2 " + recur.R2);
                            }
                            if (word[5].Contains("R3"))
                            {
                                recur.R3 = 1;
                                MessageBox.Show("r3 " + recur.R3);
                            }
                            if (word[5].Contains("R4"))
                            {
                                recur.R4 = 1;
                                MessageBox.Show("r4 " + recur.R4);
                            }
                            if (word[5].Contains("R5"))
                            {
                                recur.R5 = 1;
                                MessageBox.Show("r5 " + recur.R5);
                            }
                            if (word[5].Contains("R6"))
                            {
                                recur.R6 = 1;
                                MessageBox.Show("r5 " + recur.R6);
                            }

                            int a = int.Parse(word[2]);//trae el tamaño del proceso
                            tamaño1 += a;
 
                            {
                                MessageBox.Show("Procesos terminados, si desea seguir ejecutando agrege mas procesos");
                            }
                        }   
                        else
                        {
                            MessageBox.Show("Entro en else");

                            int wo;
                            wo = int.Parse(word[3]);//se asigna tiempo en - wo -
                            wo = wo * 1000;
                            
                            Thread.Sleep(wo);//tiempo de espera - ejecucion -

                            q.getterminar().Text += word[1] + "\r\n";//nombre del proceso

                            //transforsmar en 1 el recurso
                            if (word[5].Contains("R1"))
                            {
                                recur.R1 = 1;
                                MessageBox.Show("r1 " + recur.R1);
                            }
                            if (word[5].Contains("R2"))
                            {
                                recur.R2 = 1;
                                MessageBox.Show("r2 " + recur.R2);
                            }
                            if (word[5].Contains("R3"))
                            {
                                recur.R3 = 1;
                                MessageBox.Show("r3 " + recur.R3);
                            }
                            if (word[5].Contains("R4"))
                            {
                                recur.R4 = 1;
                                MessageBox.Show("r4 " + recur.R4);
                            }
                            if (word[5].Contains("R5"))
                            {
                                recur.R5 = 1;
                                MessageBox.Show("r5 " + recur.R5);
                            }
                            if (word[5].Contains("R6"))
                            {
                                recur.R6 = 1;
                                MessageBox.Show("r5 " + recur.R6);
                            }

                            int a = int.Parse(word[2]);//trae el tamaño del proceso
                            tamaño1 += a;
 
                            {
                                MessageBox.Show("Procesos terminados, si desea seguir ejecutando agrege mas procesos");
                            }
                        }
                        
                    }
                    {
                        q.getListo().Text = q.getNuevo().Text;
                        q.getNuevo().Text = "";

                        /*foreach (string[] word in list)
                        {
                            string fldmrf="";
                            for (int i = 0; i <word.Length;i++)
                            {
                                fldmrf += word[i];
                            }

                            MessageBox.Show(fldmrf);
                        }*/
                    }

                    // boton detener
                    q.getDet().Click += (s_, e_) =>
                    {
                        for (int i= 0 ; i < list.Count;i++ )
                        {
                            string[] word = list[i];
                            if (q.getterminar().ToString().Contains(word[1]))
                            {
                                list.RemoveAt(i);
                                i--;
                                /*string aux2,
                                a=word[0]
                                ,b=word[1]
                                ,c=word[2]
                                ,d=word[3]
                                ,e1=word[4]
                                ,f=word[5] ,a2,b2,c2,d2,e2,f2;

                                a2= q.getProcesos().ToString().Replace(a,string.Empty);
                                b2= q.getProcesos().ToString().Replace(b, string.Empty);
                                c2= q.getProcesos().ToString().Replace(c, string.Empty);
                                d2= q.getProcesos().ToString().Replace(d, string.Empty);
                                e2= q.getProcesos().ToString().Replace(e1, string.Empty);
                                f2= q.getProcesos().ToString().Replace(f, string.Empty);
                                */

                            }
                        }
                        //MessageBox.Show("hola mundo");
                        q.getProcesos().Text = "";
                        foreach (string[] word in list  )
                        {
                            string aux20="";
                            
                            q.getProcesos().Text += word[0]+"\t";
                            q.getProcesos().Text += word[1] + "\t"+"\t";
                            q.getProcesos().Text += word[2] + "Kb" +"\t" + "\t";
                            q.getProcesos().Text += word[3] + "seg" + "\t" + "\t";
                            q.getProcesos().Text += word[4]  + "\t" + "\t";
                            q.getProcesos().Text += word[5] + "\r\n";
                            for(int i = 0; i < word.Length; i++)
                            {
                                aux20 += word[i];
                            }
                            MessageBox.Show(aux20);


                        }
                        q.getbloqueados().Text=" ";
                        q.getEjecu().Text = "";
                        q.getListo().Text = "";
                        q.getNuevo().Text = " ";
                        q.getterminar().Text = " ";                      
                        q.getDet().Visible = false;
                        q.getEjec().Visible = true;
                        q.getMem().Enabled = true;
                        q.getButton().Enabled = true;
                        q.getacerca().Enabled = true;
                        

                    };


                }
            };

            q.getMem().Click += (s, e) =>
            {
                t.Show();
                t.getTamDisp().Text = "" + tamaño1;
            };
            q.getacerca().Click += (s, e) =>
            {
                u.Show();
            };
            q.getRecursos().Click += (s, e) =>
            {
                string FL;

                //recurso 1
                FL = Recursoutil(recur.R1);
                w.getr1().Text = FL;
                //recurso 2
                FL = Recursoutil(recur.R2);
                w.getr2().Text = FL;
                //recurso 3
                FL = Recursoutil(recur.R3);
                w.getr3().Text = FL;
                //recurso 4
                FL = Recursoutil(recur.R4);
                w.getr4().Text = FL;
                //recurso 5
                FL = Recursoutil(recur.R5);
                w.getr5().Text = FL;
                //recurso 6
                FL = Recursoutil(recur.R6);
                w.getr6().Text = FL;
                w.Show();

            };
            Application.Run(q);


        }
        public static String Recursoutil(int a)
        {

            string c;
            if (a == 1)
            {
                c = "Disponible";
            }
            else
            {
                c = "No Disponible";
            }

            return c;
        }

    }

}
