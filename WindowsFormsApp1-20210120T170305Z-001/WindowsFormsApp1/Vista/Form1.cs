﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Vista;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            txttiempo.Enabled= false;
            txtnuevo.Enabled = false;
            txtlisto.Enabled = false;
            txtejecutando.Enabled = false;
            txtbloqueado.Enabled = false;
            txtterminado.Enabled = false;
            listproc.Enabled = false;
            
        }

        public TextBox getNuevo() { return this.txtnuevo; }
        public TextBox getProcesos() { return this.listproc; }
        public Button getButton() { return this.btncrear; }
        public TextBox getTiempo() { return this.txttiempo; }
        public Button getMem() { return this.btnmem; }
        public Button getDet() { return this.btndetener; }
        public Button getEjec() { return this.btnejecutar; }
        public TextBox getListo() { return this.txtlisto; }
       
        public TextBox getEjecu() { return this.txtejecutando; } 
        public Button getacerca() { return this.btnacerca; }
        public Button getRecursos() { return this.btnrecursos; }
        public TextBox getbloqueados() { return this.txtbloqueado; }
        public TextBox getterminar() { return this.txtterminado; }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        

        

        

        private void btnejecutar_Click(object sender, EventArgs e)
        {

        }

        private void txtejecutando_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
