﻿namespace WindowsFormsApp1
{
    partial class   Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtnuevo = new System.Windows.Forms.TextBox();
            this.btncrear = new System.Windows.Forms.Button();
            this.listproc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtlisto = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtbloqueado = new System.Windows.Forms.TextBox();
            this.txtejecutando = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtterminado = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnejecutar = new System.Windows.Forms.Button();
            this.txttiempo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnmem = new System.Windows.Forms.Button();
            this.btndetener = new System.Windows.Forms.Button();
            this.btnacerca = new System.Windows.Forms.Button();
            this.btnrecursos = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtnuevo
            // 
            this.txtnuevo.Location = new System.Drawing.Point(82, 72);
            this.txtnuevo.Multiline = true;
            this.txtnuevo.Name = "txtnuevo";
            this.txtnuevo.Size = new System.Drawing.Size(109, 56);
            this.txtnuevo.TabIndex = 0;
            // 
            // btncrear
            // 
            this.btncrear.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncrear.Location = new System.Drawing.Point(635, 238);
            this.btncrear.Name = "btncrear";
            this.btncrear.Size = new System.Drawing.Size(111, 23);
            this.btncrear.TabIndex = 1;
            this.btncrear.Text = "Agregar Proceso";
            this.btncrear.UseVisualStyleBackColor = true;
            // 
            // listproc
            // 
            this.listproc.Location = new System.Drawing.Point(35, 262);
            this.listproc.Multiline = true;
            this.listproc.Name = "listproc";
            this.listproc.Size = new System.Drawing.Size(570, 96);
            this.listproc.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(115, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "NUEVO";
            // 
            // txtlisto
            // 
            this.txtlisto.Location = new System.Drawing.Point(258, 72);
            this.txtlisto.Multiline = true;
            this.txtlisto.Name = "txtlisto";
            this.txtlisto.Size = new System.Drawing.Size(109, 56);
            this.txtlisto.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(290, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "LISTO";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(266, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "BLOQUEADO";
            // 
            // txtbloqueado
            // 
            this.txtbloqueado.Location = new System.Drawing.Point(256, 161);
            this.txtbloqueado.Multiline = true;
            this.txtbloqueado.Name = "txtbloqueado";
            this.txtbloqueado.Size = new System.Drawing.Size(109, 56);
            this.txtbloqueado.TabIndex = 7;
            // 
            // txtejecutando
            // 
            this.txtejecutando.Location = new System.Drawing.Point(430, 72);
            this.txtejecutando.Multiline = true;
            this.txtejecutando.Name = "txtejecutando";
            this.txtejecutando.Size = new System.Drawing.Size(109, 56);
            this.txtejecutando.TabIndex = 8;
            this.txtejecutando.TextChanged += new System.EventHandler(this.txtejecutando_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(438, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "EJECUTANDO";
            // 
            // txtterminado
            // 
            this.txtterminado.Location = new System.Drawing.Point(597, 72);
            this.txtterminado.Multiline = true;
            this.txtterminado.Name = "txtterminado";
            this.txtterminado.Size = new System.Drawing.Size(109, 56);
            this.txtterminado.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(612, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 16);
            this.label5.TabIndex = 12;
            this.label5.Text = "TERMINADO";
            // 
            // btnejecutar
            // 
            this.btnejecutar.BackColor = System.Drawing.Color.Lime;
            this.btnejecutar.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnejecutar.Location = new System.Drawing.Point(644, 186);
            this.btnejecutar.Margin = new System.Windows.Forms.Padding(0);
            this.btnejecutar.Name = "btnejecutar";
            this.btnejecutar.Size = new System.Drawing.Size(95, 31);
            this.btnejecutar.TabIndex = 15;
            this.btnejecutar.Text = "Ejecutar";
            this.btnejecutar.UseVisualStyleBackColor = false;
            this.btnejecutar.Click += new System.EventHandler(this.btnejecutar_Click);
            // 
            // txttiempo
            // 
            this.txttiempo.Location = new System.Drawing.Point(380, 6);
            this.txttiempo.Name = "txttiempo";
            this.txttiempo.Size = new System.Drawing.Size(100, 20);
            this.txttiempo.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(32, 244);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 16);
            this.label7.TabIndex = 18;
            this.label7.Text = "ID";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(68, 243);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 16);
            this.label8.TabIndex = 19;
            this.label8.Text = "NOMBRE";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(164, 243);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 16);
            this.label9.TabIndex = 20;
            this.label9.Text = "TAMAÑO";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(260, 243);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 16);
            this.label10.TabIndex = 21;
            this.label10.Text = "TIEMPO";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(368, 243);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 16);
            this.label11.TabIndex = 22;
            this.label11.Text = "ESTADO";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(481, 243);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 16);
            this.label12.TabIndex = 23;
            this.label12.Text = "RECURSOS";
            // 
            // btnmem
            // 
            this.btnmem.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmem.Location = new System.Drawing.Point(615, 270);
            this.btnmem.Name = "btnmem";
            this.btnmem.Size = new System.Drawing.Size(154, 23);
            this.btnmem.TabIndex = 24;
            this.btnmem.Text = "Ver Memoria Disponible";
            this.btnmem.UseVisualStyleBackColor = true;
            // 
            // btndetener
            // 
            this.btndetener.BackColor = System.Drawing.Color.Red;
            this.btndetener.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndetener.ForeColor = System.Drawing.Color.White;
            this.btndetener.Location = new System.Drawing.Point(505, 188);
            this.btndetener.Name = "btndetener";
            this.btndetener.Size = new System.Drawing.Size(73, 29);
            this.btndetener.TabIndex = 25;
            this.btndetener.Text = "Detener";
            this.btndetener.UseVisualStyleBackColor = false;
            // 
            // btnacerca
            // 
            this.btnacerca.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnacerca.Location = new System.Drawing.Point(655, 302);
            this.btnacerca.Name = "btnacerca";
            this.btnacerca.Size = new System.Drawing.Size(75, 23);
            this.btnacerca.TabIndex = 28;
            this.btnacerca.Text = "Acerca De";
            this.btnacerca.UseVisualStyleBackColor = true;
            // 
            // btnrecursos
            // 
            this.btnrecursos.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnrecursos.Location = new System.Drawing.Point(644, 334);
            this.btnrecursos.Name = "btnrecursos";
            this.btnrecursos.Size = new System.Drawing.Size(96, 23);
            this.btnrecursos.TabIndex = 29;
            this.btnrecursos.Text = "Ver Recursos";
            this.btnrecursos.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(281, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 17);
            this.label6.TabIndex = 17;
            this.label6.Text = "Tiempo Proceso:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightYellow;
            this.ClientSize = new System.Drawing.Size(801, 370);
            this.Controls.Add(this.btnrecursos);
            this.Controls.Add(this.btnacerca);
            this.Controls.Add(this.btndetener);
            this.Controls.Add(this.btnmem);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txttiempo);
            this.Controls.Add(this.btnejecutar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtterminado);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtejecutando);
            this.Controls.Add(this.txtbloqueado);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtlisto);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listproc);
            this.Controls.Add(this.btncrear);
            this.Controls.Add(this.txtnuevo);
            this.Name = "Form1";
            this.Text = "SIMULADOR PROCESOS";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtnuevo;
        private System.Windows.Forms.Button btncrear;
        private System.Windows.Forms.TextBox listproc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtlisto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtbloqueado;
        private System.Windows.Forms.TextBox txtejecutando;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtterminado;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnejecutar;
        private System.Windows.Forms.TextBox txttiempo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnmem;
        private System.Windows.Forms.Button btndetener;
        private System.Windows.Forms.Button btnacerca;
        private System.Windows.Forms.Button btnrecursos;
        private System.Windows.Forms.Label label6;
    }
}

