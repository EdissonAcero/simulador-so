﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Proceso
    {
        private int id;
        private String nombre;
        private int tamaño;
        //private String recurso;
        private String estado;
        private int tiempo;
       
        public Proceso(int id, String nombre, int tamaño, String estado, int tiempo) //String recurso)
        {
            this.Id = id;
            this.Nombre = nombre;
            this.Tamaño = tamaño;
            this.Estado = estado;
            this.tiempo = tiempo;
            //this.recurso = recurso;
        }
        public Proceso() { }

        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public int Tamaño { get => tamaño; set => tamaño = value; }
        public string Estado { get => estado; set => estado = value; }
        public int Tiempo { get => tiempo; set => tiempo = value; }
        //public String Recursos { get => recurso; set => recurso = value; }
        
        public int getId() { return this.id; }
        public String getNom() { return this.nombre; }
        public int getTam() { return this.tamaño; }
        public int getTime() { return this.tiempo; }
        public string getEstado() { return this.estado; }
        //public String getRecurso() { return this.recurso; }
        
    }
}
